﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public PanelController activePanel;
    public bool enableUserPanel;
    public bool useSampleData;
    public bool saveData;

    public UserPanelController userPanelController;
    public LunchMenuPanelController lunchMenuPanelController;
    public LunchOptionDetailsPanelController lunchOptionDetailsPanelController;
    public CalendarPanelController calendarPanelController;
    public MonthSummaryPanelController monthSummaryPanelController;
    public LoginPanelController loginPanelController;
    public MainMenuController mainMenuController;
    public MonthFinancialSummary monthFinancialSummary;
    public ParentsMonthSummary parentFinancialSummary;
    private List<PanelController> panels;

    private IDataProvider dataProvider;
    private User currentUser;

    private void Awake() {
        if (useSampleData) {
            dataProvider = new SampleDataProvider();
        } else {
            dataProvider = new FileDataProvider();
            if (!((FileDataProvider)dataProvider).FileExist()) {
                dataProvider = new SampleDataProvider();
            }
        }

        currentUser = new User();
    }

    private void Start() {
        panels = GetComponentsInChildren<PanelController>().ToList();
        panels.ForEach(p => p.SetActive(false, false));
        userPanelController.SetActive(enableUserPanel);
        activePanel.SetActive(true); 
    }

    private void OnDestroy() {
        if (saveData) {
            dataProvider.SaveData();
        }
    }

    public void ChangePanel(PanelController newPanel, bool refreshNew = true, bool refreshOld = false) {
        activePanel.SetActive(false, refreshOld);
        newPanel.SetActive(true, refreshNew);
        activePanel = newPanel;

        if (newPanel is EditablePanelController) {
            userPanelController.EnableEditButton(((EditablePanelController)newPanel).OpenEditMode);
        } else {
            userPanelController.DisableEditButton();
        }
    }

    public void Login(User user) {
        userPanelController.SetActive(true);
        currentUser = user;
        userPanelController.userLogged.text = currentUser.Name + " " + currentUser.Surname;
        switch(user.userType)
        {
            case UserType.Accountant:
                mainMenuController.AccountantView();
                break;
            case UserType.Student:
                mainMenuController.StudentView();
                break;
            case UserType.Parent:
                mainMenuController.ParentView();
                break;
        }
    }

    public void Logout() {
        panels.ForEach(p => p.SetActive(false, false));
        ChangePanel(loginPanelController);
    }

    public DataContainer GetData() {
        return dataProvider.GetData();
    }

    public User GetUser() {
        return currentUser;
    }
}
