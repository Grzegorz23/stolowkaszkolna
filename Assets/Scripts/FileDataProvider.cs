﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class FileDataProvider : IDataProvider
{
    private DataContainer dataContainer;

    public DataContainer GetData() {
        if (dataContainer == null) {
            string filePath = Application.persistentDataPath + "/" + Constants.DefaultFilename;

            if (File.Exists(filePath)) {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream file = File.Open(filePath, FileMode.Open);
                dataContainer = (DataContainer)binaryFormatter.Deserialize(file);
                file.Close();
            } else {
                dataContainer = new DataContainer();
            }
        }

        return dataContainer;
    }

    public bool FileExist() {
        return File.Exists(Application.persistentDataPath + "/" + Constants.DefaultFilename);
    }

    public void SaveData() {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/" + Constants.DefaultFilename, FileMode.OpenOrCreate);
        binaryFormatter.Serialize(file, dataContainer);
        file.Close();
    }

    public void SetData(DataContainer data) {
        dataContainer = data;
    }
}
