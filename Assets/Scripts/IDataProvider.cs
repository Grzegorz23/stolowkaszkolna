﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataProvider
{
    void SetData(DataContainer data);
    DataContainer GetData();
    void SaveData();
}
