﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataContainer
{
    public List<LunchCategory> lunchCategories = new List<LunchCategory>();
    public List<Ingredient> ingredients = new List<Ingredient>();
    public List<LunchOption> lunchOptions = new List<LunchOption>();
    public List<LunchOrder> lunchOrders = new List<LunchOrder>();
    public List<Month> months = new List<Month>();
    public List<User> users = new List<User>();
    public List<FinancialSummary> financialSummaries = new List<FinancialSummary>();
}
