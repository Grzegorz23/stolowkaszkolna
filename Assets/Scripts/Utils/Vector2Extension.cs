﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector2Extension
{
    public static float CoordsSum(this Vector2 vector) {
        return vector.x + vector.y;
    }
}
