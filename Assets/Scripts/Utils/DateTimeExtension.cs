﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DateTimeExtension
{
    public static int DayOfWeekNumber(this DateTime dateTime) {
        return dateTime.DayOfWeek == 0 ? 7 : (int)dateTime.DayOfWeek;
    }
}
