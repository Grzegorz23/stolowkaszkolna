﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class StringExtension
{
    public static string FirstCharToUpper(this string input) { 
        return input.First().ToString().ToUpper() + input.Substring(1);
    }
}
