﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public const string DefaultFilename = "data.dat";

    #region Colors
    public static Color OuterCircleRed = new Color32(170, 0, 0, 255);
    public static Color InnerCircleRed = new Color32(255, 150, 140, 255);
    public static Color MaskCircleRed = new Color32(255, 150, 140, 100);

    public static Color OuterCircleGreen = new Color32(0, 115, 40, 255);
    public static Color InnerCircleGreen = new Color32(130, 255, 160, 255);
    public static Color MaskCircleGreen = new Color32(130, 255, 160, 100);

    public static Color OuterCircleYellow = new Color32(200, 150, 0, 255);
    public static Color InnerCircleYellow = new Color32(255, 255, 0, 255);
    public static Color MaskCircleYellow = new Color32(255, 255, 0, 100);

    public static Color GreyText = new Color32(140, 140, 140, 255);
    public static Color GreenText = new Color32(0, 115, 40, 255);
    public static Color RedText = new Color32(170, 0, 0, 255);
    #endregion Colors

    #region Labels
    public const string All = "Wszystko";

    public const string JanuaryDay = "{0}. stycznia";
    public const string FebruaryDay = "{0}. lutego";
    public const string MarchDay = "{0}. marca";
    public const string AprilDay = "{0}. kwietnia";
    public const string MayDay = "{0}. maja";
    public const string JuneDay = "{0}. czerwca";
    public const string JulyDay = "{0}. lipca";
    public const string AugustDay = "{0}. sierpnia";
    public const string SeptemberDay = "{0}. września";
    public const string OctoberDay = "{0}. października";
    public const string NovemberDay = "{0}. listopada";
    public const string DecemberDay = "{0}. grudnia";

    public const string OrderStatusOrdered = "zamówiony";
    public const string OrderStatusUsed = "odebrany";
    public const string OrderStatusLost = "nieodebrany";

    public const string CurrencyLabel = "zł";

    public const string PaymentStatusRemaining = "nierozliczony";
    public const string PaymentStatusSettled = "rozliczony";
    #endregion Labels
}
