﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngredientController : MonoBehaviour
{
    public Text ingredientName;
    public Text ingredientWeight;

    public void Initialize(Ingredient ingredient, float weight) {
        ingredientName.text = ingredient.name;
        ingredientWeight.text = weight.ToString();
    }
}
