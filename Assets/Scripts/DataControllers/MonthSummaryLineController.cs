﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonthSummaryLineController : MonoBehaviour
{
    public Text dayNumber;
    public Text orderStatus;
    public Text lunchPrice;

    public void Initialize(LunchOrder lunchOrder) {
        dayNumber.text = GetDayMonthName(lunchOrder.date.Day, lunchOrder.date.Month);
        orderStatus.text = GetLunchOrderStatusLabel(lunchOrder.status);
        lunchPrice.text = LunchOrder.LunchPrice.ToString() + " " + Constants.CurrencyLabel;
    }

    private string GetDayMonthName(int dayNumber, int monthNumber) {
        string monthText = "{0}";

        switch (monthNumber) {
            case 1:
                monthText = Constants.JanuaryDay;
                break;
            case 2:
                monthText = Constants.FebruaryDay;
                break;
            case 3:
                monthText = Constants.MarchDay;
                break;
            case 4:
                monthText = Constants.AprilDay;
                break;
            case 5:
                monthText = Constants.MayDay;
                break;
            case 6:
                monthText = Constants.JuneDay;
                break;
            case 7:
                monthText = Constants.JulyDay;
                break;
            case 8:
                monthText = Constants.AugustDay;
                break;
            case 9:
                monthText = Constants.SeptemberDay;
                break;
            case 10:
                monthText = Constants.OctoberDay;
                break;
            case 11:
                monthText = Constants.NovemberDay;
                break;
            case 12:
                monthText = Constants.DecemberDay;
                break;
        }

        return string.Format(monthText, dayNumber);
    }

    private string GetLunchOrderStatusLabel(LunchOrderStatus status) {
        switch (status) {
            case LunchOrderStatus.Ordered:
                return Constants.OrderStatusOrdered;
            case LunchOrderStatus.Used:
                return Constants.OrderStatusUsed;
            case LunchOrderStatus.Lost:
                return Constants.OrderStatusLost;
        }
        throw new ArgumentOutOfRangeException();
    }
}
