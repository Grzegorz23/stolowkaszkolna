﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalendarDayController : MonoBehaviour
{
    public Image outerCircle;
    public Image innerCircle;
    public Image square;
    public Text number;
    public Image mask;

    private UIController uiController;
    private CalendarPanelController calendarPanelController;
    private LunchOrder lunchOrder;
    private DateTime dateTime;
    private bool isCurrentMonth;
    private Button button;

    private void Awake() {
        button = GetComponent<Button>();
    }

    public void Initialize(UIController uiController, CalendarPanelController calendarPanelController, bool isCurrentMonth, DateTime dateTime, LunchOrder lunchOrder) {
        this.uiController = uiController;
        this.calendarPanelController = calendarPanelController;
        this.lunchOrder = lunchOrder;
        this.dateTime = dateTime;
        this.isCurrentMonth = isCurrentMonth;

        square.gameObject.SetActive(dateTime.Date == DateTime.Today);

        number.text = dateTime.Day.ToString();

        if (isCurrentMonth) {
            UpdateCircles();
        } else {
            number.color = Constants.GreyText;
            SetCirclesEnabled(false);
        }

        SetEditable(calendarPanelController.IsEditModeEnabled());
    }

    public bool HasLunchOrder() {
        return lunchOrder != null;
    }

    public LunchOrder GetLunchOrder() {
        return lunchOrder;
    }

    public void SetEditable(bool isEditable) {
        if (isEditable) {
            button.onClick.AddListener(ChangeOrderStatus);
        } else {
            button.onClick.RemoveAllListeners();
        }
    }

    private void SetCirclesEnabled(bool enabled) {
        outerCircle.gameObject.SetActive(enabled);
        innerCircle.gameObject.SetActive(enabled);
        mask.gameObject.SetActive(enabled);
    }

    private Color32 GetOuterCircleColor(LunchOrderStatus status) {
        switch (status) {
            case LunchOrderStatus.Lost:
                return Constants.OuterCircleRed;
            case LunchOrderStatus.Ordered:
                return Constants.OuterCircleYellow;
            case LunchOrderStatus.Used:
                return Constants.OuterCircleGreen;
        }
        throw new ArgumentOutOfRangeException();
    }

    private Color32 GetInnerCircleColor(LunchOrderStatus status) {
        switch (status) {
            case LunchOrderStatus.Lost:
                return Constants.InnerCircleRed;
            case LunchOrderStatus.Ordered:
                return Constants.InnerCircleYellow;
            case LunchOrderStatus.Used:
                return Constants.InnerCircleGreen;
        }
        throw new ArgumentOutOfRangeException();
    }

    private Color32 GetMaskCircleColor(LunchOrderStatus status) {
        switch (status) {
            case LunchOrderStatus.Lost:
                return Constants.MaskCircleRed;
            case LunchOrderStatus.Ordered:
                return Constants.MaskCircleYellow;
            case LunchOrderStatus.Used:
                return Constants.MaskCircleGreen;
        }
        throw new ArgumentOutOfRangeException();
    }

    public void ChangeOrderStatus(LunchOrderStatus status) {
        if (lunchOrder != null) {
            if (CanChangeStatus()) {
                lunchOrder.status = status;
                UpdateCircles();
            }
        } else {
            if (CanChangeStatus()) {
                CreateLunchOrder();
            }
        }
    }

    public void ChangeOrderStatus() {
        if (lunchOrder != null) {
            if (CanChangeStatus()) {
                switch (lunchOrder.status) {
                    case LunchOrderStatus.Canceled:
                        lunchOrder.status = LunchOrderStatus.Ordered;
                        UpdateCircles();
                        break;
                    case LunchOrderStatus.Ordered:
                        lunchOrder.status = LunchOrderStatus.Canceled;
                        UpdateCircles();
                        break;
                }
                calendarPanelController.OnLunchOrderStatusChanged();
            }    
        } else {
            if (CanChangeStatus()) {
                CreateLunchOrder();
                calendarPanelController.OnLunchOrderStatusChanged();
            }
        }
    }

    private void CreateLunchOrder() {
        lunchOrder = new LunchOrder {
            status = LunchOrderStatus.Ordered,
            userId = uiController.GetUser().userId,
            date = dateTime,
            paymentStatus = PaymentStatus.Remaining
        };
        calendarPanelController.GetLunchOrders().Add(lunchOrder);
        UpdateCircles();
    }

    private void UpdateCircles() {
        if (lunchOrder != null && lunchOrder.status != LunchOrderStatus.Canceled) {
            SetCirclesEnabled(true);
            outerCircle.color = GetOuterCircleColor(lunchOrder.status);
            innerCircle.color = GetInnerCircleColor(lunchOrder.status);
            mask.color = GetMaskCircleColor(lunchOrder.status);
        } else {
            SetCirclesEnabled(false);
        }
    }

    public bool CanChangeStatus() {
        return isCurrentMonth
            && (dateTime.Date > DateTime.Today
                || dateTime.Date == DateTime.Today && DateTime.Now.Hour < 8)
            && dateTime.DayOfWeekNumber() < 6;
    }
}
