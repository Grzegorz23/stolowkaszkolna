﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParentFinancialMonthController : MonoBehaviour
{
    public  Text Parent;
    private User user;
    public  Button button;
    private ParentsMonthSummary parentsMonthSummary;

    private void Awake() {
        button.onClick.AddListener(getParentReport);
    }

    private void getParentReport() {
        parentsMonthSummary.enableDropdowns(user);
    }

    public void Initialize(ParentsMonthSummary parentsMonthSummary, User user) {
        this.parentsMonthSummary = parentsMonthSummary;
        Parent.text = user.Name +" "+user.Surname;
        this.user = user;
    }
}
