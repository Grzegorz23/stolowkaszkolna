﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinancialSummaryMonthController : MonoBehaviour
{
    public Text monthName;
    private Month month;
    public Button button;

    private MonthFinancialSummary financialSummaryController;

    private void Awake() {
        button.onClick.AddListener(getFinancialReport);
    }

    private void getFinancialReport() {
        financialSummaryController.getMonthReport(month);
    }

    public void Initialize(MonthFinancialSummary financialSummaryController, Month month) {
        this.financialSummaryController = financialSummaryController;
        monthName.text = month.name;
        this.month = month;
    }
}
