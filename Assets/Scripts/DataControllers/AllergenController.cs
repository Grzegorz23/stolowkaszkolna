﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllergenController : MonoBehaviour
{
    public Text allergenName;

    public void Initialize(string name) {
        allergenName.text = name;
    }
}
