﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LunchCategoryController : MonoBehaviour
{
    public Text lunchCategoryName;
    public Button button;

    private LunchMenuPanelController lunchMenuPanelController;

    private void Awake() {
        button.onClick.AddListener(FilterLunchOptions);
    }

    private void FilterLunchOptions() {
        lunchMenuPanelController.FilterLunchOptions(lunchCategoryName.text);
    }

    public void Initialize(LunchMenuPanelController lunchMenuPanelController, LunchCategory lunchCategory) {
        this.lunchMenuPanelController = lunchMenuPanelController;
        lunchCategoryName.text = lunchCategory.name;
    }
}
