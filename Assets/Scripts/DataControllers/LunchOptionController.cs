﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LunchOptionController : MonoBehaviour
{
    public Text lunchOptionName;
    public Text lunchOptionDescription;
    public Button button;

    private UIController uiController;
    private LunchOption lunchOption;

    private void Awake() {
        button.onClick.AddListener(OpenLunchOptionDetailsPanel);
    }

    private void OpenLunchOptionDetailsPanel() {
        uiController.lunchOptionDetailsPanelController.SetLunchOption(lunchOption);
        uiController.ChangePanel(uiController.lunchOptionDetailsPanelController);
    }

    public void Initialize(UIController uiController, LunchOption lunchOption) {
        this.uiController = uiController;
        this.lunchOption = lunchOption;

        lunchOptionName.text = lunchOption.name;
        lunchOptionDescription.text = lunchOption.description;
    }
}
