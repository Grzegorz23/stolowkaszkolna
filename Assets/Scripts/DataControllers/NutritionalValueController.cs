﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NutritionalValueController : MonoBehaviour
{
    public Text nutritionalValuePer100;
    public Text nutritionalValuePerServing;

    public void Initialize(List<KeyValuePair<Ingredient, float>> ingredients, Func<Ingredient, float> ingredientNutritionalValueFunc) {
        float nutritionalValueSumPerServing = ingredients.Sum(i => ingredientNutritionalValueFunc(i.Key) * i.Value / 100);

        nutritionalValuePer100.text = (100 * nutritionalValueSumPerServing / ingredients.Sum(i => i.Value)).ToString("0.00");
        nutritionalValuePerServing.text = nutritionalValueSumPerServing.ToString("0.00");
    }
}
