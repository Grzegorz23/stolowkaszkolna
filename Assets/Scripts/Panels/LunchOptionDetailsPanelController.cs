﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LunchOptionDetailsPanelController : PanelController
{
    public Button backButton;

    public Text lunchCategoryName;
    public Text lunchOptionName;
    public RectTransform dataContainer;
    public RectTransform ingredientTitle;
    public RectTransform ingredientHeader;
    public RectTransform ingredientSpace;
    public IngredientController ingredientPrefab;
    public NutritionalValueController nutritionalValueEnergy;
    public NutritionalValueController nutritionalValueFat;
    public NutritionalValueController nutritionalValueSaturatedFat;
    public NutritionalValueController nutritionalValueCarbohydrates;
    public NutritionalValueController nutritionalValueSugars;
    public NutritionalValueController nutritionalValueProtein;
    public NutritionalValueController nutritionalValueFibre;
    public NutritionalValueController nutritionalValueSalt;
    public RectTransform allergenSpace;
    public RectTransform allergenTitle;
    public AllergenController allergenPrefab;

    private UIController uiController;
    private LunchOption lunchOption;

    private void Start() {
        uiController = GetComponentInParent<UIController>();
        backButton.onClick.AddListener(OpenLunchMenuPanel);
    }

    private void OpenLunchMenuPanel() {
        uiController.ChangePanel(uiController.lunchMenuPanelController, false, false);
    }

    public void SetLunchOption(LunchOption lunchOption) {
        this.lunchOption = lunchOption;
    }

    private void Clear() {
        dataContainer.GetComponentsInChildren<IngredientController>().ToList().ForEach(x => Destroy(x.gameObject));
        dataContainer.GetComponentsInChildren<AllergenController>().ToList().ForEach(x => Destroy(x.gameObject));
    }

    protected override void Refresh() {
        Clear();

        if (lunchOption.ingredientWeights.Count > 0) {
            ingredientTitle.gameObject.SetActive(true);
            ingredientHeader.gameObject.SetActive(true);
            ingredientSpace.gameObject.SetActive(true);

            for (int i = 0; i < lunchOption.ingredientWeights.Count; i++) {
                KeyValuePair<Ingredient, float> ingredientWeight = lunchOption.ingredientWeights[i];

                IngredientController ingredientController = Instantiate(ingredientPrefab);
                ingredientController.Initialize(ingredientWeight.Key, ingredientWeight.Value);

                RectTransform rectTransform = ingredientController.GetComponent<RectTransform>();

                rectTransform.SetParent(dataContainer);
                rectTransform.SetSiblingIndex(ingredientHeader.GetSiblingIndex() + i + 1);
                rectTransform.localScale = new Vector3(1, 1, 1);
            }
        } else {
            ingredientTitle.gameObject.SetActive(false);
            ingredientHeader.gameObject.SetActive(false);
            ingredientSpace.gameObject.SetActive(false);
        }
        
        nutritionalValueEnergy.Initialize(lunchOption.ingredientWeights, i => i.energy);
        nutritionalValueFat.Initialize(lunchOption.ingredientWeights, i => i.fat);
        nutritionalValueSaturatedFat.Initialize(lunchOption.ingredientWeights, i => i.saturatedFat);
        nutritionalValueCarbohydrates.Initialize(lunchOption.ingredientWeights, i => i.carbohydrates);
        nutritionalValueSugars.Initialize(lunchOption.ingredientWeights, i => i.sugars);
        nutritionalValueProtein.Initialize(lunchOption.ingredientWeights, i => i.protein);
        nutritionalValueFibre.Initialize(lunchOption.ingredientWeights, i => i.fibre);
        nutritionalValueSalt.Initialize(lunchOption.ingredientWeights, i => i.salt);

        List<string> allergens = lunchOption.ingredientWeights.SelectMany(i => i.Key.allergens).Distinct().ToList();
        if (allergens.Count > 0) {
            allergenSpace.gameObject.SetActive(true);
            allergenTitle.gameObject.SetActive(true);

            for (int i = 0; i < allergens.Count; i++) {
                AllergenController allergenController = Instantiate(allergenPrefab);
                allergenController.Initialize(allergens[i]);

                RectTransform rectTransform = allergenController.GetComponent<RectTransform>();

                rectTransform.SetParent(dataContainer);
                rectTransform.SetSiblingIndex(allergenTitle.GetSiblingIndex() + i + 1);
                rectTransform.localScale = new Vector3(1, 1, 1);
            }
        } else {
            allergenSpace.gameObject.SetActive(false);
            allergenTitle.gameObject.SetActive(false);
        }
    }
}
