﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : PanelController
{
    public Button calendar;
    public Button lunchMenu;
    public Button monthSummary;
    public Button monthFinancialSummary;
    public Button parentFinancialSummary;

    private UIController uiController;

    private void Start()
    {
        uiController = GetComponentInParent<UIController>();
        calendar.onClick.AddListener(OpenCalendar);
        lunchMenu.onClick.AddListener(OpenLunchMenu);
        monthSummary.onClick.AddListener(OpenMonthSummary);
        monthFinancialSummary.onClick.AddListener(OpenMonthFinancialSummary);
        parentFinancialSummary.onClick.AddListener(OpenParentFinancialSummary);
    }

    public void AccountantView()
    {
        lunchMenu.gameObject.SetActive(false);
    }

    public void ParentView()
    {
        monthFinancialSummary.gameObject.SetActive(false);
        parentFinancialSummary.gameObject.SetActive(false);
    }

    public void StudentView()
    {
        monthFinancialSummary.gameObject.SetActive(false);
        monthSummary.gameObject.SetActive(false);
        parentFinancialSummary.gameObject.SetActive(false);
    }

    private void OpenCalendar()
    {
        uiController.ChangePanel(uiController.calendarPanelController);
    }
    private void OpenLunchMenu()
    {
        uiController.ChangePanel(uiController.lunchMenuPanelController);
    }

    private void OpenMonthSummary()
    {
        uiController.ChangePanel(uiController.monthSummaryPanelController);
    }

    private void OpenMonthFinancialSummary()
    {
        uiController.ChangePanel(uiController.monthFinancialSummary);
    }

    private void OpenParentFinancialSummary()
    {
        uiController.ChangePanel(uiController.parentFinancialSummary);
    }

}
