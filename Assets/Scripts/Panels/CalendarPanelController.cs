﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CalendarPanelController : EditablePanelController
{
    public Button monthSummaryButton;
    public Button previousMonthButton;
    public Button nextMonthButton;

    public Text yearMonthTitle;
    public CalendarDayController calendarDayPrefab;
    public RectTransform calendarContainer;
    public RectTransform firstWeek;
    public RectTransform secondWeek;
    public RectTransform thirdWeek;
    public RectTransform fourthWeek;
    public RectTransform fifthWeek;
    public RectTransform sixthWeek;
    public Toggle markAllToggle;

    private UIController uiController;
    private List<LunchOrder> lunchOrders;

    private DateTime selectedDate;

    private void Awake() {
        uiController = GetComponentInParent<UIController>();
        monthSummaryButton.onClick.AddListener(OpenMonthSummaryPanel);
        previousMonthButton.onClick.AddListener(() => ChangeMonth(-1));
        nextMonthButton.onClick.AddListener(() => ChangeMonth(1));
        markAllToggle.onValueChanged.AddListener(MarkAllRemainingDays);
        markAllToggle.gameObject.SetActive(false);
        lunchOrders = uiController.GetData().lunchOrders;
    }

    private void OpenMonthSummaryPanel() {
        uiController.ChangePanel(uiController.monthSummaryPanelController);
        uiController.monthSummaryPanelController.SetDate(selectedDate);
    }

    private List<CalendarDayController> GetCalendarDayControllers() {
        List<CalendarDayController> calendarDayControllers = new List<CalendarDayController>();
        calendarDayControllers.AddRange(firstWeek.GetComponentsInChildren<CalendarDayController>().ToList());
        calendarDayControllers.AddRange(secondWeek.GetComponentsInChildren<CalendarDayController>().ToList());
        calendarDayControllers.AddRange(thirdWeek.GetComponentsInChildren<CalendarDayController>().ToList());
        calendarDayControllers.AddRange(fourthWeek.GetComponentsInChildren<CalendarDayController>().ToList());
        calendarDayControllers.AddRange(fifthWeek.GetComponentsInChildren<CalendarDayController>().ToList());
        calendarDayControllers.AddRange(sixthWeek.GetComponentsInChildren<CalendarDayController>().ToList());
        return calendarDayControllers.Where(x => x.gameObject.activeSelf).ToList();
    }

    private void Clear() {
        GetCalendarDayControllers().ForEach(x => x.gameObject.SetActive(false));
        GetCalendarDayControllers().ForEach(x => Destroy(x.gameObject));
    }

    protected void Refresh(bool setCurrentMonth) {
        Clear();
        
        if (setCurrentMonth) {
            selectedDate = DateTime.Now;
        }

        GenerateCalendar();
    }

    protected override void Refresh() {
        Refresh(true);
    }

    private void GenerateCalendar() {
        DateTime current = new DateTime(selectedDate.Year, selectedDate.Month, 1);

        yearMonthTitle.text = current.ToString("MMMM", new CultureInfo("pl-pl")).FirstCharToUpper() + " " + current.ToString("yyyy");

        current = current.AddDays(-(current.DayOfWeekNumber() - 1));

        for (int i = 1; i <= 6; i++) {
            for (int j = 0; j < 7; j++) {
                bool isDayFromCurrentMonth = current.Month == selectedDate.Month;
                LunchOrder lunchOrder = lunchOrders.FirstOrDefault(o => o.date.Date == current.Date);

                CalendarDayController calendarDayController = Instantiate(calendarDayPrefab);
                calendarDayController.Initialize(uiController, this, isDayFromCurrentMonth, current, lunchOrder);

                RectTransform rectTransform = calendarDayController.GetComponent<RectTransform>();

                rectTransform.SetParent(GetDayContainer(i));
                rectTransform.localScale = new Vector3(1, 1, 1);

                current = current.AddDays(1);
            }
        }

        UpdateMarkAllToggle();
    }

    public void ChangeMonth(int change) {
        selectedDate = selectedDate.AddMonths(change);
        Refresh(false);
    }

    public void SetDate(DateTime date) {
        selectedDate = date;
        Refresh(false);
    }

    private RectTransform GetDayContainer(int weekNumber) {
        switch (weekNumber) {
            case 1:
                return firstWeek;
            case 2:
                return secondWeek;
            case 3:
                return thirdWeek;
            case 4:
                return fourthWeek;
            case 5:
                return fifthWeek;
            case 6:
                return sixthWeek;
        }
        return null;
    }

    private void MarkAllRemainingDays(bool mark) {
        GetCalendarDayControllers()
            .Where(d => d.CanChangeStatus())
            .ToList()
            .ForEach(d => d.ChangeOrderStatus(mark ? LunchOrderStatus.Ordered : LunchOrderStatus.Canceled));
    }

    private void SetCalendarDaysEditable(bool editable) {
        GetCalendarDayControllers().ForEach(x => x.SetEditable(editable));
    }

    public override void OpenEditMode() {
        base.OpenEditMode();

        uiController.userPanelController.EnableEditMode(SaveAndCloseEditMode, CloseEditMode);
        SetCalendarDaysEditable(true);
        monthSummaryButton.gameObject.SetActive(false);
        lunchOrders = uiController.GetData().lunchOrders.Select(x => new LunchOrder(x)).ToList();
        Refresh(false);
    }

    public void SaveAndCloseEditMode() {
        uiController.GetData().lunchOrders = lunchOrders;
        CloseEditMode();
    }

    public override void CloseEditMode() {
        base.CloseEditMode();

        lunchOrders = uiController.GetData().lunchOrders;

        uiController.userPanelController.DisableEditMode();
        SetCalendarDaysEditable(false);
        monthSummaryButton.gameObject.SetActive(true);
        Refresh(false);
    }

    public List<LunchOrder> GetLunchOrders() {
        return lunchOrders;
    }

    public void OnLunchOrderStatusChanged() {
        UpdateMarkAllToggle();
    }

    private void UpdateMarkAllToggle() {
        List<CalendarDayController> editableDays = GetCalendarDayControllers().Where(x => x.CanChangeStatus()).ToList();

        if (editableDays.Count > 0 && isEditModeEnabled) {
            markAllToggle.gameObject.SetActive(true);

            markAllToggle.onValueChanged.RemoveAllListeners();
            markAllToggle.isOn = editableDays.All(x => x.HasLunchOrder() && x.GetLunchOrder().status != LunchOrderStatus.Canceled);
            markAllToggle.onValueChanged.AddListener(MarkAllRemainingDays);
        } else {
            markAllToggle.gameObject.SetActive(false);
        } 
    }
}
