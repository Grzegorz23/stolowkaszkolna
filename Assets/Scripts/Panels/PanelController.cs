﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PanelController : MonoBehaviour
{
    protected virtual void Refresh() { }

    public void SetActive(bool active, bool refresh = true) {
        gameObject.SetActive(active);

        if (refresh) {
            Refresh();
        }
    }
}
