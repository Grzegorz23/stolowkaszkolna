﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EditablePanelController : PanelController
{
    protected bool isEditModeEnabled;

    public virtual void OpenEditMode() {
        isEditModeEnabled = true;
    }

    public virtual void CloseEditMode() {
        isEditModeEnabled = false;
    }

    public bool IsEditModeEnabled() {
        return isEditModeEnabled;
    }
}
