﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MonthSummaryPanelController : PanelController
{
    public Button backButton;
    public Button previousMonthButton;
    public Button nextMonthButton;

    public Text yearMonthTitle;
    public RectTransform summaryLineContainer;
    public RectTransform monthSummaryHeader;
    public MonthSummaryLineController monthSummaryLinePrefab;
    public Text monthCost;
    public Text unsettledOrders;
    public Text unsettledAmount;
    public Text paymentStatus;

    private UIController uiController;

    private DateTime selectedDate;

    private void Awake() {
        uiController = GetComponentInParent<UIController>();
        backButton.onClick.AddListener(OpenCalendarPanel);
        previousMonthButton.onClick.AddListener(() => ChangeMonth(-1));
        nextMonthButton.onClick.AddListener(() => ChangeMonth(1));
    }

    private void OpenCalendarPanel() {
        uiController.ChangePanel(uiController.calendarPanelController, false);
        uiController.calendarPanelController.SetDate(selectedDate);
    }

    private void Clear() {
        summaryLineContainer.GetComponentsInChildren<MonthSummaryLineController>().ToList().ForEach(x => Destroy(x.gameObject));
    }

    protected override void Refresh() {
        Clear();

        selectedDate = DateTime.Now;

        GenerateMonthSummary();
    }

    public void ChangeMonth(int change) {
        selectedDate = selectedDate.AddMonths(change);

        Clear();

        GenerateMonthSummary();
    }

    public void SetDate(DateTime date) {
        selectedDate = date;

        Clear();

        GenerateMonthSummary();
    }

    private void GenerateMonthSummary() {
        yearMonthTitle.text = selectedDate.ToString("MMMM", new CultureInfo("pl-pl")).FirstCharToUpper() + " " + selectedDate.ToString("yyyy");

        List<LunchOrder> lunchOrders = uiController.GetData().lunchOrders
            .Where(o => o.date.Year == selectedDate.Year 
                && o.date.Month == selectedDate.Month 
                && o.status != LunchOrderStatus.Canceled
                && o.userId == uiController.GetUser().userId)
            .OrderBy(o => o.date)
            .ToList();

        for (int i = 0; i < lunchOrders.Count; i++) {
            MonthSummaryLineController monthSummaryLineController = Instantiate(monthSummaryLinePrefab);
            monthSummaryLineController.Initialize(lunchOrders[i]);

            RectTransform rectTransform = monthSummaryLineController.GetComponent<RectTransform>();

            rectTransform.SetParent(summaryLineContainer);
            rectTransform.SetSiblingIndex(monthSummaryHeader.GetSiblingIndex() + i + 1);
            rectTransform.localScale = new Vector3(1, 1, 1);
        }

        monthCost.text = (lunchOrders.Count * LunchOrder.LunchPrice).ToString() + " " + Constants.CurrencyLabel;

        int unsettledOrderCount = lunchOrders.Where(o => o.paymentStatus == PaymentStatus.Remaining).Count();

        unsettledOrders.text = unsettledOrderCount.ToString();
        unsettledAmount.text = (unsettledOrderCount * LunchOrder.LunchPrice).ToString() + " " + Constants.CurrencyLabel;
        paymentStatus.text = unsettledOrderCount > 0 ? Constants.PaymentStatusRemaining : Constants.PaymentStatusSettled;
        paymentStatus.color = unsettledOrderCount > 0 ? Constants.RedText : Constants.GreenText;
    }
}
