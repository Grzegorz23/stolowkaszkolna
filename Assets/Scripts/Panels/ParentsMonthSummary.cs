﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ParentsMonthSummary : PanelController
{
    public int sidePanelMovingSteps;
    public float sidePanelMovingDelay;
    public RectTransform sidePanel;
    public Button showHideSidePanelButton;
    public InputField searchInput;


    public RectTransform parentsContainer;
    public ParentFinancialMonthController parentPrefab;
    public Dropdown yearDropdown;
    public Dropdown monthDropdown;

    private Coroutine moveSidePanelCoroutine;
    private float sidePanelHidePosition;


    //public Text searchParentText;
    public Text amountToPay;
    public Text paidAmount;
    public Text reportTitle;

    private User choosenUser;
    private bool yearIsChoosen;
    private bool monthIsChoosen;
    private UIController uiController;

    private void Awake()
    {
        uiController = GetComponentInParent<UIController>();
    }

    private void Start()
    {
        sidePanelHidePosition = sidePanel.anchoredPosition.x;
        showHideSidePanelButton.onClick.AddListener(() => SetSidePanelShown(!IsSidePanelShown()));
        monthDropdown.onValueChanged.AddListener(monthChoosen);
        yearDropdown.onValueChanged.AddListener(yearChoosen);
        searchInput.onValueChanged.AddListener(filterParentList);

        yearDropdown.gameObject.SetActive(false);
        monthDropdown.gameObject.SetActive(false);

        Refresh();
    }

    private void SetSidePanelShown(bool shown)
    {
        if (moveSidePanelCoroutine == null)
        {
            Vector2 targetPosition = new Vector2(shown ? 0 : sidePanelHidePosition, sidePanel.anchoredPosition.y);
            moveSidePanelCoroutine = StartCoroutine(MoveElementToPositionCoroutine(sidePanel, targetPosition, sidePanelMovingSteps, sidePanelMovingDelay));
        }
    }

    private bool IsSidePanelShown()
    {
        return sidePanel.anchoredPosition.x == 0;
    }

    private IEnumerator MoveElementToPositionCoroutine(RectTransform element, Vector2 position, int steps, float timeDelay)
    {
        Vector2 step = (position - element.anchoredPosition) / steps;
        while (element.anchoredPosition != position)
        {
            if ((step.CoordsSum() < 0 && element.anchoredPosition.CoordsSum() < position.CoordsSum()) ||
                    (step.CoordsSum() >= 0 && element.anchoredPosition.CoordsSum() > position.CoordsSum()))
            {
                element.anchoredPosition = position;
            }
            else
            {
                element.anchoredPosition = element.anchoredPosition + step;
            }
            yield return new WaitForSecondsRealtime(timeDelay);
        }
        moveSidePanelCoroutine = null;
    }

    private void Clear()
    {
        clearParents();
        ClearParentSummary();
    }

    private void clearParents()
    {
        parentsContainer.GetComponentsInChildren<ParentFinancialMonthController>().ToList().ForEach(x => Destroy(x.gameObject));
    }

    private void ClearParentSummary()
    {
        amountToPay.text = "";
        paidAmount.text = "";
    }
    
    public void filterParentList(string filterValue = "")
    {
        Refresh();
    }

    protected override void Refresh()
    {
        Clear();
        foreach (User user in uiController.GetData().users.Where(u => u.userType == UserType.Parent &&
        (searchInput.text != "" ? (u.Name + " " + u.Surname).Contains(searchInput.text) : true)
        ))
        {
            ParentFinancialMonthController parentFinancialMonthController = Instantiate(parentPrefab);
            parentFinancialMonthController.Initialize(this, user);

            RectTransform rectTransform = parentFinancialMonthController.GetComponent<RectTransform>();

            rectTransform.SetParent(parentsContainer);
            rectTransform.localScale = new Vector3(1, 1, 1);
        }
    }



    public void monthChoosen(int val)
    {
        monthIsChoosen = true;
        if(monthIsChoosen && yearIsChoosen)
        {
            int year = int.Parse(yearDropdown.options[yearDropdown.value].text);
            int month = monthDropdown.value + 1;
            CalculateSummary(month, year);
        }
    }
    
    public void yearChoosen(int val)
    {
        yearIsChoosen = true;
        if (monthIsChoosen && yearIsChoosen)
        {
            int year = int.Parse(yearDropdown.options[yearDropdown.value].text);
            int month = monthDropdown.value + 1;
            CalculateSummary(month,year);
        }
    }

    public void CalculateSummary(int month, int year)
    {
        double paidAmount = 0;
        double amountToPay = 0;

        reportTitle.text = monthDropdown.options[monthDropdown.value].text + " " + yearDropdown.options[yearDropdown.value].text;
        List<LunchOrder> orders = uiController.GetData().lunchOrders.Where(o => o.date.Month == month && o.date.Year == year && o.userId == choosenUser.userId).ToList<LunchOrder>();

        foreach (LunchOrder order in orders)
        {
            switch (order.paymentStatus)
            {
                case PaymentStatus.Remaining:
                    amountToPay += LunchOrder.LunchPrice;
                    break;
                case PaymentStatus.Settled:
                    paidAmount += LunchOrder.LunchPrice;
                    break;

            }
        }

        this.amountToPay.text = amountToPay.ToString() + " PLN";
        this.paidAmount.text = paidAmount.ToString() + " PLN";

    }

    public void enableDropdowns(User user)
    {
        ClearParentSummary();

        choosenUser = user;
        yearDropdown.gameObject.SetActive(true);
        monthDropdown.gameObject.SetActive(true);

        monthIsChoosen = false;
        monthDropdown.value = 0;
        yearIsChoosen = false;
        yearDropdown.value = 0;
    }
}
