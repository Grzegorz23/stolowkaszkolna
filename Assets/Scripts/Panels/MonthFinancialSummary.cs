﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MonthFinancialSummary : PanelController
{
    public int sidePanelMovingSteps;
    public float sidePanelMovingDelay;
    public RectTransform sidePanel;
    public Button showHideSidePanelButton;
    public Button calculateSummary;

    public RectTransform monthsContainer;
    public FinancialSummaryMonthController monthPrefab;
    public Dropdown yearDropdown;

    private Coroutine moveSidePanelCoroutine;
    private float sidePanelHidePosition;
    private Month currentMonth;

    public Text totalAmount;
    public Text canceledAmount;
    public Text returnAmount;
    public Text notReturnAmount;
    public Text reportTitle;

    private UIController uiController;

    private void Awake()
    {
        uiController = GetComponentInParent<UIController>();
    }

    private void Start()
    {
        sidePanelHidePosition = sidePanel.anchoredPosition.x;
        showHideSidePanelButton.onClick.AddListener(() => SetSidePanelShown(!IsSidePanelShown()));
        calculateSummary.onClick.AddListener(CalculateSummary);

        calculateSummary.gameObject.SetActive(false);
        Refresh();
    }

    private void SetSidePanelShown(bool shown)
    {
        if (moveSidePanelCoroutine == null)
        {
            Vector2 targetPosition = new Vector2(shown ? 0 : sidePanelHidePosition, sidePanel.anchoredPosition.y);
            moveSidePanelCoroutine = StartCoroutine(MoveElementToPositionCoroutine(sidePanel, targetPosition, sidePanelMovingSteps, sidePanelMovingDelay));
        }
    }

    private bool IsSidePanelShown()
    {
        return sidePanel.anchoredPosition.x == 0;
    }

    private IEnumerator MoveElementToPositionCoroutine(RectTransform element, Vector2 position, int steps, float timeDelay)
    {
        Vector2 step = (position - element.anchoredPosition) / steps;
        while (element.anchoredPosition != position)
        {
            if ((step.CoordsSum() < 0 && element.anchoredPosition.CoordsSum() < position.CoordsSum()) ||
                    (step.CoordsSum() >= 0 && element.anchoredPosition.CoordsSum() > position.CoordsSum()))
            {
                element.anchoredPosition = position;
            }
            else
            {
                element.anchoredPosition = element.anchoredPosition + step;
            }
            yield return new WaitForSecondsRealtime(timeDelay);
        }
        moveSidePanelCoroutine = null;
    }

    private void Clear()
    {
        clearMonths();
        ClearMonthSummary();
    }

    private void clearMonths()
    {
        monthsContainer.GetComponentsInChildren<FinancialSummaryMonthController>().ToList().ForEach(x => Destroy(x.gameObject));
    }

    private void ClearMonthSummary()
    {
        totalAmount.text = "";
        canceledAmount.text = "";
        returnAmount.text = "";
        notReturnAmount.text = "";
}

    protected override void Refresh()
    {
        Clear();

        foreach (Month month in uiController.GetData().months)
        {
            FinancialSummaryMonthController financialMonthController = Instantiate(monthPrefab);
            financialMonthController.Initialize(this, month);

            RectTransform rectTransform = financialMonthController.GetComponent<RectTransform>();

            rectTransform.SetParent(monthsContainer);
            rectTransform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void CalculateSummary()
    {
        double totalAmount = 0;
        double canceledAmount = 0;
        double returnAmount = 0;
        double notReturnAmount = 0;

        calculateSummary.gameObject.SetActive(false);

        int year = int.Parse(yearDropdown.options[yearDropdown.value].text);

        reportTitle.text = currentMonth.name + " " + yearDropdown.options[yearDropdown.value].text;
        List<LunchOrder> orders = uiController.GetData().lunchOrders.Where(o => o.date.Month == currentMonth.number && o.date.Year == year).ToList<LunchOrder>();

        foreach (LunchOrder order in orders)
        {
            switch (order.status)
            {
                case LunchOrderStatus.Used:
                    totalAmount += LunchOrder.LunchPrice;
                    break;
                case LunchOrderStatus.Lost:
                    totalAmount += LunchOrder.LunchPrice;
                    canceledAmount++;
                    notReturnAmount++;
                    break;
                case LunchOrderStatus.Canceled:
                    canceledAmount++;
                    returnAmount++;
                    break;
            }
        }
        FinancialSummary summary = new FinancialSummary
        {
            totalAmount = totalAmount.ToString(),
            returnAmount = returnAmount.ToString(),
            canceledAmount = canceledAmount.ToString(),
            notReturnAmount = notReturnAmount.ToString(),
            monthNumber = currentMonth.number,
            year = year
            
        };
        uiController.GetData().financialSummaries.Add(summary);
        this.returnAmount.text = returnAmount.ToString() + " PLN";
        this.totalAmount.text = totalAmount.ToString() + " PLN";
        this.canceledAmount.text = canceledAmount.ToString() + " PLN";
        this.notReturnAmount.text = notReturnAmount.ToString() + " PLN";
    }

    public void getMonthReport(Month month)
    {
        ClearMonthSummary();
        currentMonth = month;

        int year = int.Parse(yearDropdown.options[yearDropdown.value].text);

        FinancialSummary theSummary = uiController.GetData().financialSummaries.Where(s => s.monthNumber == month.number && s.year == year).FirstOrDefault();

        if (theSummary == null)
        {
            calculateSummary.gameObject.SetActive(true);
        }
        else
        {
            calculateSummary.gameObject.SetActive(false);
            reportTitle.text = month.name + " " + yearDropdown.options[yearDropdown.value].text;
            returnAmount.text = theSummary.returnAmount;
            totalAmount.text = theSummary.totalAmount;
            canceledAmount.text = theSummary.canceledAmount;
            notReturnAmount.text = theSummary.notReturnAmount;
        }
    }



    //private void InstantiateLunchOptions(List<LunchOption> lunchOptions)
    //{
    //    foreach (LunchOption lunchOption in lunchOptions)
    //    {
    //        LunchOptionController lunchOptionController = Instantiate(lunchOptionPrefab);
    //        lunchOptionController.Initialize(uiController, lunchOption);
    //        lunchOptionController.GetComponent<RectTransform>().SetParent(lunchOptionContainer);
    //    }
    //}
}
