﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

public class LoginPanelController : PanelController
{
    [SerializeField]
    public Text login;
    public InputField password2;
    public Button LoginButton;
    private UIController uiController;


    EventSystem system;

    void Start()
    {
        system = EventSystem.current;
        uiController = GetComponentInParent<UIController>();
        LoginButton.onClick.AddListener(LogInApp);

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();

            if (next != null)
            {

                InputField inputfield = next.GetComponent<InputField>();
                if (inputfield != null)
                    inputfield.OnPointerClick(new PointerEventData(system));

                system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
            }
        }
    }

    private void LogInApp()
    {
        User user = uiController.GetData().users.Where(u => u.userLogin == login.text && u.userPassword == password2.text).ToList().FirstOrDefault();
        if (user != null)
        {
            uiController.Login(user);
            uiController.ChangePanel(uiController.mainMenuController);
        }
    }
}
