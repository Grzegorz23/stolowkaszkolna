﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UserPanelController : PanelController
{
    public Button homeButton;
    public Button logoutButton;
    public Text userLogged;
    public Button editButton;
    public Button acceptButton;
    public Button cancelButton;

    private UIController uiController;

    private void Start() {
        uiController = GetComponentInParent<UIController>();
        homeButton.onClick.AddListener(OpenHomePanel);
        logoutButton.onClick.AddListener(uiController.Logout);
    }

    public void OpenHomePanel() {
        uiController.ChangePanel(uiController.mainMenuController);
    }

    private void SetEditModeEnabled(bool enabled) {
        homeButton.gameObject.SetActive(!enabled);
        logoutButton.gameObject.SetActive(!enabled);
        userLogged.gameObject.SetActive(!enabled);
        editButton.gameObject.SetActive(!enabled);
        acceptButton.gameObject.SetActive(enabled);
        cancelButton.gameObject.SetActive(enabled);
    }

    public void EnableEditMode(UnityAction acceptAction, UnityAction cancelAction) {
        SetEditModeEnabled(true);
        acceptButton.onClick.AddListener(acceptAction);
        cancelButton.onClick.AddListener(cancelAction);
    }

    public void DisableEditMode() {
        SetEditModeEnabled(false);
        acceptButton.onClick.RemoveAllListeners();
        cancelButton.onClick.RemoveAllListeners();
    }

    public void EnableEditButton(UnityAction onClickAction) {
        editButton.gameObject.SetActive(true);
        editButton.onClick.AddListener(onClickAction);
    }

    public void DisableEditButton() {
        editButton.gameObject.SetActive(false);
        editButton.onClick.RemoveAllListeners();
    }
}
