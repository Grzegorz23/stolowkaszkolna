﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LunchMenuPanelController : PanelController
{
    public int sidePanelMovingSteps;
    public float sidePanelMovingDelay;
    public RectTransform sidePanel;
    public Button showHideSidePanelButton;

    public RectTransform lunchCategoryContainer;
    public Button allCategoriesButton;
    public LunchCategoryController lunchCategoryPrefab;
    public Text lunchCategoryTitle;

    public RectTransform lunchOptionContainer;
    public LunchOptionController lunchOptionPrefab;

    private Coroutine moveSidePanelCoroutine;
    private float sidePanelHidePosition;

    private UIController uiController;

    private void Awake() {
        uiController = GetComponentInParent<UIController>();
    }

    private void Start() {
        sidePanelHidePosition = sidePanel.anchoredPosition.x;
        showHideSidePanelButton.onClick.AddListener(() => SetSidePanelShown(!IsSidePanelShown()));
        allCategoriesButton.onClick.AddListener(ShowAllLunchOptions);
        Refresh();
    }

    private void SetSidePanelShown(bool shown) {
        if (moveSidePanelCoroutine == null) {
            Vector2 targetPosition = new Vector2(shown ? 0 : sidePanelHidePosition, sidePanel.anchoredPosition.y);
            moveSidePanelCoroutine = StartCoroutine(MoveElementToPositionCoroutine(sidePanel, targetPosition, sidePanelMovingSteps, sidePanelMovingDelay));
        }
    }

    private bool IsSidePanelShown() {
        return sidePanel.anchoredPosition.x == 0;
    }

    private IEnumerator MoveElementToPositionCoroutine(RectTransform element, Vector2 position, int steps, float timeDelay) {
        Vector2 step = (position - element.anchoredPosition) / steps;
        while (element.anchoredPosition != position) {
            if ((step.CoordsSum() < 0 && element.anchoredPosition.CoordsSum() < position.CoordsSum()) ||
                    (step.CoordsSum() >= 0 && element.anchoredPosition.CoordsSum() > position.CoordsSum())) {
                element.anchoredPosition = position;
            } else {
                element.anchoredPosition = element.anchoredPosition + step;
            }
            yield return new WaitForSecondsRealtime(timeDelay);
        }
        moveSidePanelCoroutine = null;
    }

    private void Clear() {
        ClearLunchCategories();
        ClearLunchOptions();
    }

    private void ClearLunchCategories() {
        lunchCategoryContainer.GetComponentsInChildren<LunchCategoryController>().ToList().ForEach(x => Destroy(x.gameObject));
    }

    private void ClearLunchOptions() {
        lunchOptionContainer.GetComponentsInChildren<LunchOptionController>().ToList().ForEach(x => Destroy(x.gameObject));
    }

    protected override void Refresh() {
        Clear();

        foreach (LunchCategory lunchCategory in uiController.GetData().lunchCategories) {
            LunchCategoryController lunchCategoryController = Instantiate(lunchCategoryPrefab);
            lunchCategoryController.Initialize(this, lunchCategory);

            RectTransform rectTransform = lunchCategoryController.GetComponent<RectTransform>();

            rectTransform.SetParent(lunchCategoryContainer);
            rectTransform.localScale = new Vector3(1, 1, 1);
        }

        ShowAllLunchOptions();
    }

    public void FilterLunchOptions(string lunchCategoryName) {
        ClearLunchOptions();

        lunchCategoryTitle.text = lunchCategoryName;

        List<LunchOption> filterLunchOptions = uiController.GetData().lunchOptions.Where(o => o.categories.Any(c => c.name == lunchCategoryName)).ToList();
        InstantiateLunchOptions(filterLunchOptions);
    }

    private void ShowAllLunchOptions() {
        ClearLunchOptions();

        lunchCategoryTitle.text = Constants.All;

        InstantiateLunchOptions(uiController.GetData().lunchOptions);
    }

    private void InstantiateLunchOptions(List<LunchOption> lunchOptions) {
        foreach (LunchOption lunchOption in lunchOptions) {
            LunchOptionController lunchOptionController = Instantiate(lunchOptionPrefab);
            lunchOptionController.Initialize(uiController, lunchOption);

            RectTransform rectTransform = lunchOptionController.GetComponent<RectTransform>();

            rectTransform.SetParent(lunchOptionContainer);
            rectTransform.localScale = new Vector3(1, 1, 1);
        }
    }
}
