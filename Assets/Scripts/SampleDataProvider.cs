﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleDataProvider : IDataProvider
{
    private DataContainer dataContainer;

    public DataContainer GetData() {
        if (dataContainer == null) {
            dataContainer = new DataContainer {
                lunchCategories = GenerateLunchCategories(),
                ingredients = GenerateIngredients(),
                months = GenerateMonths(),
                lunchOrders = GenerateLunchOrders(),
                users = GenerateUsers()
            };
            dataContainer.lunchOptions = GenerateLunchOptions(dataContainer.lunchCategories, dataContainer.ingredients);
        }

        return dataContainer;
    }

    public void SetData(DataContainer data) {
        dataContainer = data;
    }

    public void SaveData() {
        FileDataProvider fileDataProvider = new FileDataProvider();
        fileDataProvider.SetData(dataContainer);
        fileDataProvider.SaveData();
    }

    public List<User> GenerateUsers()
    {
        List<User> users = new List<User>
        {
            new User
            {
                userId = 0,
                userType = UserType.Admin,
                userLogin = "admin",
                userPassword = "admin",
                Name = "Benek",
                Surname = "Dzbenek"
            },
            new User
            {
                userId = 1,
                userType = UserType.Parent,
                userLogin = "parent1",
                userPassword = "parent1",
                Name = "Rodzic1",
                Surname = "Nazwisko1"
            },
            new User
            {
                userId = 7,
                userType = UserType.Parent,
                userLogin = "parent2",
                userPassword = "parent2",
                Name = "Rodzic2",
                Surname = "Nazwisko2"
            },
            new User
            {
                userId = 8,
                userType = UserType.Parent,
                userLogin = "parent3",
                userPassword = "parent3",
                Name = "Rodzic3",
                Surname = "Nazwisko3"
            },
            new User
            {
                userId = 9,
                userType = UserType.Parent,
                userLogin = "parent4",
                userPassword = "parent4",
                Name = "Rodzic4",
                Surname = "Nazwisko4"
            },
            new User
            {
                userId = 2,
                userType = UserType.Accountant,
                userLogin = "accountant2",
                userPassword = "accountant2",
                Name = "Józef",
                Surname = "Przekupny"
            },
            new User
            {
                userId = 3,
                userType = UserType.Student,
                userLogin = "stud3",
                userPassword = "stud3",
                Name = "Jaś",
                Surname = "Iek"
            },
            new User
            {
                userId = 4,
                userType = UserType.Student,
                userLogin = "stud4",
                userPassword = "stud4",
                Name = "Marcin",
                Surname = "Wun"
            },
            new User
            {
                userId = 5,
                userType = UserType.Student,
                userLogin = "stud5",
                userPassword = "stud5",
                Name = "Jestem",
                Surname = "Uczniem"
            },
            new User
            {
                userId = 6,
                userType = UserType.Student,
                userLogin = "stud6",
                userPassword = "stud6",
                Name = "How",
                Surname = "How"
            },

        };
        return users;
    }

    public List<LunchCategory> GenerateLunchCategories() {
        List<LunchCategory> lunchCategories = new List<LunchCategory> {
            new LunchCategory {
                name = "Dania mięsne"
            },
            new LunchCategory {
                name = "Dania wegetariańskie"
            },
            new LunchCategory {
                name = "Zupy"
            }
        };

        return lunchCategories;
    }

    public List<Month> GenerateMonths()
    {
        List<Month> months = new List<Month>
        {
            new Month{name = "Styczeń",
                      number = 1 },
            new Month{name = "Luty",
                      number = 2 },
            new Month{name = "Marzec",
                      number = 3 },
            new Month{name = "Kwiecień",
                      number = 4 },
            new Month{name = "Maj",
                      number = 5 },
            new Month{name = "Czerwiec",
                      number = 6 },
            new Month{name = "Lipiec",
                      number = 7 },
            new Month{name = "Sierpień",
                      number = 8 },
            new Month{name = "Wrzesień",
                      number = 9 },
            new Month{name = "Październik",
                      number = 10 },
            new Month{name = "Listopad",
                      number = 11 },
            new Month{name = "Grudzień",
                      number = 12 },
        };
        return months;
    }

    public List<Ingredient> GenerateIngredients() {
        List<Ingredient> ingredients = new List<Ingredient> {
            new Ingredient {
                name = "Mięso wieprzowe",
                showOnList = true,
                energy = 327,
                fat = 8.7f,
                saturatedFat = 3.5f,
                carbohydrates = 11.4f,
                sugars = 10.8f,
                protein = 50.5f,
                fibre = 0.5f,
                salt = 1.3f
            },
            new Ingredient {
                name = "Ziemniaki",
                showOnList = true,
                energy = 77,
                fat = 0,
                saturatedFat = 0,
                carbohydrates = 0.17f,
                sugars = 0,
                protein = 0.02f,
                fibre = 0.02f,
                salt = 0.06f
            },
            new Ingredient {
                name = "Seler",
                showOnList = true,
                energy = 42,
                fat = 0.3f,
                saturatedFat = 0.08f,
                carbohydrates = 9.2f,
                sugars = 1.6f,
                protein = 1.5f,
                fibre = 1.8f,
                salt = 0.01f,
                allergens = new List<string> {
                    "Seler"
                } 
            },
            new Ingredient {
                name = "Biała kapusta",
                showOnList = true,
                energy = 25,
                fat = 0.1f,
                saturatedFat = 0.034f,
                carbohydrates = 5.8f,
                sugars = 0f,
                protein = 1.28f,
                fibre = 2.5f,
                salt = 0.018f
            },
            new Ingredient {
                name = "Szpinak",
                showOnList = true,
                energy = 23,
                fat = 0.39f,
                saturatedFat = 0.063f,
                carbohydrates = 3.63f,
                sugars = 0f,
                protein = 2.86f,
                fibre = 2.2f,
                salt = 0.079f
            },
            new Ingredient {
                name = "Ciasto pierogowe",
                showOnList = false,
                energy = 230.86f,
                fat = 2.82f,
                saturatedFat = 0,
                carbohydrates = 45.9f,
                sugars = 0f,
                protein = 5.97f,
                fibre = 0,
                salt = 0
            },
            new Ingredient {
                name = "Kalafior",
                showOnList = true,
                energy = 23,
                fat = 0.45f,
                saturatedFat = 0,
                carbohydrates = 4.11f,
                sugars = 2.08f,
                protein = 1.84f,
                fibre = 0,
                salt = 0.015f
            },
            new Ingredient {
                name = "Marchew",
                showOnList = true,
                energy = 41,
                fat = 0.24f,
                saturatedFat = 0,
                carbohydrates = 9.58f,
                sugars = 4.74f,
                protein = 0.93f,
                fibre = 2.8f,
                salt = 0.069f
            }
        };

        return ingredients;
    }

    public List<LunchOption> GenerateLunchOptions(List<LunchCategory> lunchCategories, List<Ingredient> ingredients) {
        List<LunchOption> lunchOptions = new List<LunchOption> {
            new LunchOption {
                name = "Kotlet schabowy",
                description = "mięso wieprzowe, ziemniaki, surówka",
                categories = new List<LunchCategory> {
                    lunchCategories[0]
                },
                ingredientWeights = new List<KeyValuePair<Ingredient, float>> {
                    new KeyValuePair<Ingredient, float>(ingredients[0], 100),
                    new KeyValuePair<Ingredient, float>(ingredients[1], 150),
                    new KeyValuePair<Ingredient, float>(ingredients[2], 30)
                }
            },
            new LunchOption {
                name = "Kotlet mielony",
                description = "mięso wieprzowe, ziemniaki, surówka",
                categories = new List<LunchCategory> {
                    lunchCategories[0]
                },
                ingredientWeights = new List<KeyValuePair<Ingredient, float>> {
                    new KeyValuePair<Ingredient, float>(ingredients[0], 100),
                    new KeyValuePair<Ingredient, float>(ingredients[1], 150),
                    new KeyValuePair<Ingredient, float>(ingredients[3], 30)
                }
            },
            new LunchOption {
                name = "Pierogi ze szpinakiem",
                description = "ciasto pierogowe, szpinak",
                categories = new List<LunchCategory> {
                    lunchCategories[1]
                },
                ingredientWeights = new List<KeyValuePair<Ingredient, float>> {
                    new KeyValuePair<Ingredient, float>(ingredients[4], 100),
                    new KeyValuePair<Ingredient, float>(ingredients[5], 200),
                }
            },
            new LunchOption {
                name = "Zupa kalafiorowa",
                description = "kalafior, marchew, ziemniaki",
                categories = new List<LunchCategory> {
                    lunchCategories[2]
                },
                ingredientWeights = new List<KeyValuePair<Ingredient, float>> {
                    new KeyValuePair<Ingredient, float>(ingredients[6], 30),
                    new KeyValuePair<Ingredient, float>(ingredients[7], 30),
                    new KeyValuePair<Ingredient, float>(ingredients[1], 50),
                }
            }
        };

        return lunchOptions;
    }

    public List<LunchOrder> GenerateLunchOrders() {
        List<LunchOrder> lunchCategories = new List<LunchOrder> {
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 1,
                date = new DateTime(2019, 9, 5),
                paymentStatus = PaymentStatus.Remaining
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 1,
                date = new DateTime(2019, 9, 10),
                paymentStatus = PaymentStatus.Remaining

            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 1,
                date = new DateTime(2019, 9, 2),
                paymentStatus = PaymentStatus.Settled
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 1,
                date = new DateTime(2019, 9, 3),
                paymentStatus = PaymentStatus.Settled
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 1,
                date = new DateTime(2019, 9, 5),
                paymentStatus = PaymentStatus.Remaining
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 1,
                date = new DateTime(2019, 9, 9),
                paymentStatus = PaymentStatus.Remaining
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 9, 12)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 9, 13)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 9, 17)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 9, 4)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 9, 11)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 9, 26)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 10, 3)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 10, 10)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 10, 17)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 10, 24)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 10, 31)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 10, 7)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 10, 15)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 10, 23)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 10, 25)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 11, 1)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 11, 7)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 11, 13)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 11, 19)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 11, 25)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 11, 4)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 11, 12)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 11, 20)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 11, 28)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 11, 22)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 12, 2)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 12, 6)
            },
            new LunchOrder {
                status = LunchOrderStatus.Lost,
                userId = 0,
                date = new DateTime(2019, 12, 10)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 12, 4)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 12, 5)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 12, 9)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 12, 11)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 12, 12)
            },
            new LunchOrder {
                status = LunchOrderStatus.Used,
                userId = 0,
                date = new DateTime(2019, 12, 13)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2019, 12, 17)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2019, 12, 18)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2019, 12, 19)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2019, 12, 20)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2020, 1, 13)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2020, 1, 15)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2020, 1, 17)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2020, 1, 21)
            },
            new LunchOrder {
                status = LunchOrderStatus.Ordered,
                userId = 0,
                date = new DateTime(2020, 1, 23)
            }
        };

        return lunchCategories;
    }
}
