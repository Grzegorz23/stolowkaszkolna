﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
///Inactive - 
///Ordered - zamówiony
///Used - odebrany
///Lost - Zamówiony, nieodebrany i nie anulowany przed 8 danego dnia (przepadł)
///Rescheduled - przeniesiony conajmniej raz na inny termin w innym miesiącu.
///Canceled - odwołany i nie przeniesiony
/// </summary>
public enum LunchOrderStatus
{
    Canceled,
    Ordered,
    Used,
    Lost
}
