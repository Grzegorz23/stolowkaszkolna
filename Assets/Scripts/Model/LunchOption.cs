﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LunchOption
{
    public string name;
    public string description;
    public List<LunchCategory> categories = new List<LunchCategory>();
    public List<KeyValuePair<Ingredient, float>> ingredientWeights = new List<KeyValuePair<Ingredient, float>>();
}
