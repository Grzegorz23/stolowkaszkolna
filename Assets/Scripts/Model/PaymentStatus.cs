﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// </summary>
public enum PaymentStatus
{
    Settled,
    Remaining
}
