﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ingredient
{
    public string name;
    public bool showOnList;

    public float energy;
    public float fat;
    public float saturatedFat;
    public float carbohydrates;
    public float sugars;
    public float protein;
    public float fibre;
    public float salt;

    public List<string> allergens = new List<string>();
}
