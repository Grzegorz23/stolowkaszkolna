﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum UserType
{
    Student,
    Accountant,
    Parent,
    Admin
}
