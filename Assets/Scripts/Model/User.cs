﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class User
{
    // used by lunch orders
    public int userId;
    public UserType userType;
    public string userLogin;
    public string userPassword;
    public string Name;
    public string Surname;

}
