﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FinancialSummary
{
    public int monthNumber;
    public int year;
    public string totalAmount;
    public string canceledAmount;
    public string returnAmount;
    public string rescheduledAmount;
    public string notReturnAmount;
}
