﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LunchOrder
{
    public static float LunchPrice = 9;

    public LunchOrderStatus status;
    public int userId;
    public DateTime date;
    public PaymentStatus paymentStatus;

    public LunchOrder() { }

    public LunchOrder(LunchOrder other) {
        status = other.status;
        userId = other.userId;
        date = other.date;
        paymentStatus = other.paymentStatus;
    }
}
